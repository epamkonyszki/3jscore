https://docs.google.com/document/d/1bYrCSEPD3NGc_d2YS5a1FW60URrrPSl3cLSzc0-tuVs/edit#heading=h.jtrxj5fv69gc

3JS Core
Goal:
Get familiar with core language concepts: data types, scopes, hoisting, closures
Play around with unit testing
Practice
Part 1
Implement a function that calculates an expression written in reverse polish notation:
function takes single argument (string) and returns calculated value
Supported operands: integer numbers
Supported operators: “+”, “-”, “*”
Use Node to run your solution
Things to consider:
Use regex to distinguish operands and operators
Throw exception if found an error in input string
Use any syntax you prefer: ES5 (classical JS) or ES6

Part 2
Unit-test your solution with Mocha
https://mochajs.org/
Note: do not worry if there are would be some unclear things related to Node, we’ll familiarize with it later.
Part 3
Add second (optional) argument to the function: extraOperators
E.g. if extra operator ‘%’ (remainder after division) is provided, the input string “123%+” should results into 3.
Note: depending on your implementation, Object.assign could be used to merge operators
Unit test this modification
Materials
JavaScript and the ECMAScript specification
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Introduction#JavaScript_and_the_ECMAScript_Specification
Grammar and Types
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_Types
Attention to: 
Variable scope
Variable hoisting
Function hoisting
Global variables
Control flow and error handling
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling 
Attention to: Exception handling statements
Loops and iteration
Attention to:
for...in statement
for...of statement
Functions
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
Attention to:
Function expressions
Function scope
Scope and the function stack
Nested functions and closures
Name conflicts
Closures
Using the arguments object
Arrow functions
Predefined functions
Regular Expressions
https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions 




Tools
Chrome DevTools Console
ES features support 
http://kangax.github.io/compat-table/es6/
Mocha
https://mochajs.org/