var assert = require('assert');
var fs = require('fs');
var vm = require('vm');
var path = './index.js';

var code = fs.readFileSync(path);
vm.runInThisContext(code);

describe('Function  GetResultOfExpression', function() {
  var tests = [
    {arg: "( 2 + 3 ) * 5",       expected: 25},
    {arg: "5 * ( 2 + 3 )",    expected: 25}
  ];

    tests.forEach(test => {
    it('Tests GetResultOfExpression', function() {
      var res = GetResultOfExpression(test.arg);
      assert.equal(res, test.expected);
    });
  });
});

describe('Function  GetResultOfRPN', function() {
  var tests = [
    {arg: "12 2 3 4 * 10 5 / + * +",       expected: 40},
    {arg: "5 1 2 + 4 * + 3 -",    expected: 14}
  ];

    tests.forEach(test => {
    it('Tests GetResultOfRPN', function() {
      var res = GetResultOfRPN(test.arg);
      assert.equal(res, test.expected);
    });
  });
});



