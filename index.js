function isStringCorrect(expression)
{
    var reg = RegExp('[0-9*%()+\-\s]*$')
    var isCorrect = reg.test(expression);
    return isCorrect;
}


let yard = (infix) => {
  let ops = {'+': 1, '-': 1, '*': 2, '/': 2, '%':2};
  let peek = (a) => a[a.length - 1];
  let stack = [];

    if(!isStringCorrect(infix))
    {

        throw "Incorrect string";
    }

  return infix
    .split(' ')
    .reduce((output, token) => {
      if (parseFloat(token)) {
        output.push(token);
      }

      if (token in ops) {
        while (peek(stack) in ops && ops[token] <= ops[peek(stack)])
          output.push(stack.pop());
        stack.push(token);
      }

      if (token == '(') {
        stack.push(token);
      }

      if (token == ')') {
        while (peek(stack) != '(')
          output.push(stack.pop());
        stack.pop();
      }

      return output;
    }, [])
    .concat(stack.reverse())
    .join(' ');
};

function GetResultOfRPN( input ) {
  var ar = input.split(' '), st = [], token;
  while( token = ar.shift() ) { 
    if ( token == +token ) {
      st.push( token );
    } else {
      var n2 = st.pop(), n1 = st.pop();
      var re = /^[\+\-\/\*\%]$/;
      if( n1 != +n1 || n2 != +n2 || !re.test( token ) ) {
        throw new Error( 'Invalid expression: ' + input );
      }
      st.push( eval( n1 + token + ' ' + n2 ) );
    }
  }
  if( st.length !== 1 ) {
    throw new Error( 'Invalid expression: ' + input );
  }
  return st.pop();
}


function GetResultOfExpression(expression)
{
  return GetResultOfRPN(yard(expression));
}


console.log(yard("( ( 2 + 3 ) * 5 - 2 ) % 3"));


console.log(GetResultOfExpression("( ( 2 + 3 ) * 5 - 2 ) % 3"));